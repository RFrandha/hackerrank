package com.rfrandha.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
public class TrainingApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrainingApplication.class, args);

        List<List<Integer>> arr = new ArrayList<>(Arrays.asList(
                new ArrayList<>(Arrays.asList(11, 2, 4)),
                new ArrayList<>(Arrays.asList(4, 5, 6)),
                new ArrayList<>(Arrays.asList(10, 8, -12))
        ));

        System.out.println(diagonalDifference(arr));

        int[] candles = new int[]{3,2,1,3};
        System.out.println(birthdayCakeCandles(candles));

        System.out.println(timeConversion("12:05:45AM"));
    }

    private static int diagonalDifference(List<List<Integer>> arr) {
        int sum = 0;
        for(int i = 0; i < arr.size(); i++){
            sum +=  (arr.get(i).get(i) - arr.get(i).get((arr.size()-1)-i));
        }
        return Math.abs(sum);
    }

    private static int birthdayCakeCandles(int[] ar) {
        int max = Arrays.stream(ar).max().getAsInt();
        return Math.toIntExact(Arrays.stream(ar).filter(i -> i == max).count());
    }

    private static String timeConversion(String s) {
        String[] splitedTime = s.split(":");
        char prefix = splitedTime[2].charAt(2);
        splitedTime[2] = splitedTime[2].substring(0,2);

        if(prefix == 'P') {
            if(!splitedTime[0].equalsIgnoreCase("12")){
                splitedTime[0] = String.valueOf(Integer.parseInt(splitedTime[0]) + 12);
            }
        } else if(splitedTime[0].equalsIgnoreCase("12")){
            splitedTime[0] = "00";
        }

        return String.join(":", splitedTime);
    }

}